# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.18)
# Database: bxm_metadata
# Generation Time: 2017-12-03 13:24:43 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table lc_city_th
# ------------------------------------------------------------

DROP TABLE IF EXISTS `lc_city_th`;

CREATE TABLE `lc_city_th` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `city_name_local` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

INSERT INTO `lc_city_th` (`id`, `city_name`, `city_name_local`)
VALUES
	(5475068, 'BANGKOK', 'กรุงเทพมหานคร   '),
	(5475069, 'SAMUTPRAKARN', 'สมุทรปราการ   '),
	(5475070, 'NONTHABURI', 'นนทบุรี   '),
	(5475071, 'PATHUMTHANI', 'ปทุมธานี   '),
	(5475072, 'AYUTTHAYA', 'พระนครศรีอยุธยา   '),
	(5475073, 'ANG THONG', 'อ่างทอง   '),
	(5475074, 'LOP BURI', 'ลพบุรี   '),
	(5475075, 'SING BURI', 'สิงห์บุรี   '),
	(5475076, 'CHAI NAT', 'ชัยนาท   '),
	(5475077, 'SARABURI', 'สระบุรี'),
	(5475078, 'CHONBURI', 'ชลบุรี   '),
	(5475079, 'RAYONG', 'ระยอง   '),
	(5475080, 'CHANTHABURI', 'จันทบุรี   '),
	(5475081, 'TRAT', 'ตราด   '),
	(5475082, 'CHACHOENGSAO', 'ฉะเชิงเทรา   '),
	(5475083, 'PRACHINBURI', 'ปราจีนบุรี   '),
	(5475084, 'NAKHONNAYOK', 'นครนายก   '),
	(5475085, 'Sa Kaew', 'สระแก้ว      '),
	(5475086, 'NAKHON RATCHASIMA', 'นครราชสีมา   '),
	(5475087, 'BURIRAM', 'บุรีรัมย์   '),
	(5475088, 'SURIN', 'สุรินทร์   '),
	(5475089, 'SI SAKET', 'ศรีสะเกษ   '),
	(5475090, 'UBON RATCHATHANI', 'อุบลราชธานี   '),
	(5475091, 'YASOTHON', 'ยโสธร   '),
	(5475092, 'CHAIYAPHUM', 'ชัยภูมิ   '),
	(5475093, 'AMNAT CHAROEN', 'อำนาจเจริญ   '),
	(5475094, 'NONG BUA LAM PHU', 'หนองบัวลำภู   '),
	(5475095, 'KHONKAEN', 'ขอนแก่น   '),
	(5475096, 'Udon Thani', 'อุดรธานี   '),
	(5475097, 'LOEI', 'เลย   '),
	(5475098, 'NONGKHAI', 'หนองคาย   '),
	(5475099, 'MAHA SARAKHAM', 'มหาสารคาม   '),
	(5475100, 'ROIET', 'ร้อยเอ็ด   '),
	(5475101, 'KALASIN', 'กาฬสินธุ์   '),
	(5475102, 'SAKONNAKHON', 'สกลนคร   '),
	(5475103, 'NAKHON PHANOM', 'นครพนม   '),
	(5475104, 'MUKDAHAN', 'มุกดาหาร   '),
	(5475105, 'CHIANG MAI', 'เชียงใหม่   '),
	(5475106, 'LAMPHUN', 'ลำพูน   '),
	(5475107, 'LAMPANG', 'ลำปาง   '),
	(5475108, 'UTTARADIT', 'อุตรดิตถ์   '),
	(5475109, 'PHRAE', 'แพร่   '),
	(5475110, 'NAN', 'น่าน   '),
	(5475111, 'PHAYAO', 'พะเยา   '),
	(5475112, 'CHIANG RAI', 'เชียงราย   '),
	(5475113, 'MAE HONG SON', 'แม่ฮ่องสอน   '),
	(5475114, 'NAKHON SAWAN', 'นครสวรรค์   '),
	(5475115, 'UTHAI THANI', 'อุทัยธานี   '),
	(5475116, 'KAMPHAENGPHET', 'กำแพงเพชร   '),
	(5475117, 'TAK', 'ตาก   '),
	(5475118, 'SUKHOTHAI', 'สุโขทัย   '),
	(5475119, 'PHITSANULOK', 'พิษณุโลก   '),
	(5475120, 'PHICHIT', 'พิจิตร   '),
	(5475121, 'PHETCHABUN', 'เพชรบูรณ์   '),
	(5475122, 'RATCHABURI', 'ราชบุรี   '),
	(5475123, 'KANCHANABURI', 'กาญจนบุรี   '),
	(5475124, 'SUPHAN BURI', 'สุพรรณบุรี   '),
	(5475125, 'Nakhon Pathom', 'นครปฐม'),
	(5475127, 'SAMUTSONGKHRAM', 'สมุทรสงคราม   '),
	(5475128, 'PHETCHABURI', 'เพชรบุรี   '),
	(5475129, 'PRACHUAP KHIRI KHAN', 'ประจวบคีรีขันธ์   '),
	(5475130, 'NAKHON SI THAMMARAT', 'นครศรีธรรมราช   '),
	(5475131, 'KRABI', 'กระบี่   '),
	(5475132, 'PHANGNGA', 'พังงา   '),
	(5475133, 'PHUKET', 'ภูเก็ต   '),
	(5475134, 'SURAT THANI', 'สุราษฎร์ธานี   '),
	(5475135, 'RANONG', 'ระนอง   '),
	(5475136, 'CHUMPHON', 'ชุมพร   '),
	(5475137, 'SONGKHLA', 'สงขลา   '),
	(5475138, 'SATUN', 'สตูล   '),
	(5475139, 'TRANG', 'ตรัง   '),
	(5475140, 'Phatthalung', 'พัทลุง'),
	(5475141, 'PATTANI', 'ปัตตานี   '),
	(5475142, 'YALA', 'ยะลา   '),
	(5475143, 'NARATHIWAT', 'นราธิวาส   '),
	(5475144, 'BUENG KAN', 'บึงกาฬ');



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
